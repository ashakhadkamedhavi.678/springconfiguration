package com.example.springconfiguration.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
    @GetMapping("/register")
    public String getLoginpage(){
        return "login";
    }
    @GetMapping("/home")
    public String getHomepage(){
        return "home";
    }
}
